# README #

This is a university project and is the result of data collection and codes from different sources.

### Dependencies ###

* Tensorflow
* Keras
* OpenCV

### Dataset ###

* https://www.kaggle.com/datamunge/sign-language-mnist

### Tools ###

* Google Colab: https://colab.research.google.com

## How to run ##

* Create model: ModelBuilder2.ipynb
* Run model: Recogniter.py